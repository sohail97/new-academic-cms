<?php include __DIR__.'/../../../../views/includes/header.php' ?>
<body>
  <?php include __DIR__.'/../../../../views/includes/sidebar.php' ?>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <?php include __DIR__.'/../../../../views/includes/topbar.php' ?>
    <!-- Header -->
    <!-- Header -->
    <div class="col-md-12">
      <div class="card-wrapper">
        <div class="card">
          <!-- Card header -->
          <div class="card-header">
            <h3 class="mb-0">Footer Navigation</h3>
          </div>
          <!-- Card body -->
          <div class="card-body">
            <form>
              <div class="row">
                
                
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="form-group">
                      <label class="form-control-label">Page</label>
                      <select class="form-control  form-control-md" id="exampleFormControlSelect1">
                      <option value="" selected hidden>Select Type</option>
                      <option value="essaywriting">Essay Writing</option>
                      <option value="assignmentwriting">Assignment Writing</option>
                    </select>                    
                  </div>
                  </div>
                </div>
                 <div class="col-md-6">
                  <div class="form-group">
                    <div class="form-group">
                      <label class="form-control-label">Type</label>
                      <select class="form-control  form-control-md" id="exampleFormControlSelect1">
                      <option value="" selected hidden>Select Type</option>
                      <option value="essaywriting">Useful Links</option>
                      <option value="assignmentwriting">Services</option>
                      <option value="assignmentwriting">Policies</option>
                    </select>                    
                  </div>
                  </div>
                </div>
              </div>
             
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <div class="form-group">
                      <label class="form-control-label">Active</label>
                      <label class="custom-toggle">
                        <input type="checkbox">
                        <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                </div>
                <div class="col-md-6">                  
                  <button type="button" class="btn btn-outline-success float-right">Add</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- Page content -->
      <div class="container-fluid mt--6">
        
        
        
        <?php //include __DIR__.'/../../../../views/includes/footer.php' ?>
      </div>
    </div>
    <!-- Argon Scripts -->
    <!-- Core -->
    <?php include __DIR__.'/../../../../views/includes/scripts.php' ?>
  </body>
</html>