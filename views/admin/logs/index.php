<?php include __DIR__.'/../../../views/includes/header.php' ?>
<body>
  <?php include __DIR__.'/../../../views/includes/sidebar.php' ?>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <?php include __DIR__.'/../../../views/includes/topbar.php' ?>
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
        <div class="col-md-12">
          <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
              <h3 class="mb-0">Logs</h3>
            </div>
            <!-- Light table -->
            <div class="table-responsive" data-toggle="list" data-list-values='["name", "budget", "status", "completion"]'>
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col" class="sort" data-sort="name">#</th>
                    <th scope="col" class="sort" data-sort="budget">User</th>
                    <th scope="col" class="sort" data-sort="status">Role</th>
                    <th scope="col" class="sort" data-sort="status">Action</th>
                    <th scope="col" class="sort" data-sort="completion">IP</th>
                    <th scope="col" class="sort" data-sort="completion">Created At</th>
                  </tr>
                </thead>
                <tbody class="list">
                  <tr>
                    <th scope="row">
                      <div class="media align-items-center">
                      <!--   <a href="#" class="avatar rounded-circle mr-3">
                          <img alt="Image placeholder" src="<?php echo $base_url; ?>assets/img/theme/bootstrap.jpg">
                        </a> -->
                        <div class="media-body">
                          <span class="name mb-0 text-sm">1</span>
                        </div>
                      </div>
                    </th>
                    <td >
                      XYZ
                    </td>
                    <td>
                     <p>Writer</p>
                    </td>
                    <td>
                      <p>
                       Ayaz has updated Page: terms-and-conditions with ID: 19  
                      </p>
                    </td>
                 <td>
                    <p>110.37.218.94  </p>
                 </td>
                    <td>
                    <p>2020-07-09 07:53:49</p>
                    </td>
                  </tr>
                 
               
                </tbody>
              </table>
            </div>
            <!-- Card footer -->
            <div class="card-footer py-4">
              <nav aria-label="...">
                <ul class="pagination justify-content-end mb-0">
                  <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1">
                      <i class="fas fa-angle-left"></i>
                      <span class="sr-only">Previous</span>
                    </a>
                  </li>
                  <li class="page-item active">
                    <a class="page-link" href="#">1</a>
                  </li>
                  <li class="page-item">
                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item">
                    <a class="page-link" href="#">
                      <i class="fas fa-angle-right"></i>
                      <span class="sr-only">Next</span>
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
            </div>
          <!-- Card stats -->
         
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
     
      
      
      <?php include __DIR__.'/../../../views/includes/footer.php' ?>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <?php include __DIR__.'/../../../views/includes/scripts.php' ?>
</body>
</html>