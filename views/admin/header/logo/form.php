<?php include __DIR__.'/../../../../views/includes/header.php' ?>
<body>
  <?php include __DIR__.'/../../../../views/includes/sidebar.php' ?>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <?php include __DIR__.'/../../../../views/includes/topbar.php' ?>
    <!-- Header -->
    <!-- Header -->
    <div class="col-md-12">
      <div class="card-wrapper">
        <div class="card">
          <!-- Card header -->
          <div class="card-header">
            <h3 class="mb-0">Add New Logo</h3>
          </div>
          <!-- Card body -->
          <div class="card-body">
            <form>
              <div class="row">
                
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="form-control-label">Alt Text</label>
                    <input class="form-control form-control-sm" type="text" placeholder="alternate text">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="form-control-label">Active</label>
                    <label class="custom-toggle">
                      <input type="checkbox">
                      <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                    </label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="dropzone dropzone-single mb-3" data-toggle="dropzone" data-dropzone-url="http://">
                    <div class="fallback">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="projectCoverUploads">
                        <label class="custom-file-label" for="projectCoverUploads">Choose file</label>
                      </div>
                    </div>
                    <div class="dz-preview dz-preview-single">
                      <div class="dz-preview-cover">
                        <img class="dz-preview-img" src="..." alt="..." data-dz-thumbnail>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <button type="button" class="btn btn-outline-success float-right">Save</button>
            </form>
          </div>
        </div>
      </div>
      <!-- Page content -->
      <div class="container-fluid mt--6">
        
        
        
        <?php //include __DIR__.'/../../../../views/includes/footer.php' ?>
      </div>
    </div>
    <!-- Argon Scripts -->
    <!-- Core -->
    <?php include __DIR__.'/../../../../views/includes/scripts.php' ?>
  </body>
</html>