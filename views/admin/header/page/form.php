<?php include __DIR__.'/../../../../views/includes/header.php' ?>
<body>
  <?php include __DIR__.'/../../../../views/includes/sidebar.php' ?>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <?php include __DIR__.'/../../../../views/includes/topbar.php' ?>
    <!-- Header -->
    <!-- Header -->
    <div class="col-md-12">
      <div class="card-wrapper">
        <div class="card">
          <!-- Card header -->
          <div class="card-header">
            <h3 class="mb-0">Contact Details</h3>
          </div>
          <!-- Card body -->
          <div class="card-body">
            <form>
              <div class="row">
                
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="form-control-label">Page Name <small>(Without '.php'. Spaces will be replaced with '-')</small></label>
                    <input class="form-control form-control-md" type="text" placeholder="Report Writing">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="form-group">
                      <label class="form-control-label">Page Type</label>
                    <select class="form-control  form-control-md" id="exampleFormControlSelect1">
                      <option value="" selected hidden>Select Type</option>
                      <option value="0">Hidden</option>
                      <option value="1">Policies</option>
                      <option value="2">Services</option>
                      <option value="3">Useful links</option>
                      <option value="4">Others</option>
                    </select>
                    </div>
                  </div>
                </div>
              </div>
             
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="form-control-label" for="summernote">Content</label>
                    <textarea class="form-control" id="summernote" rows="10"></textarea>
                  </div>
                </div>
              </div>

               <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="form-control-label" for="m-title">Meta Title</label>
                    <textarea class="form-control" id="m-title" rows="5"></textarea>
                   <small>Character Count:<span>34</span></small>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="form-control-label" for="m-description">Meta Description </label>
                    <textarea class="form-control" id="m-description" rows="5"></textarea>
                    <small>Character Count:<span>34</span></small>
                  </div>
                </div>
              </div> 

               <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="form-control-label" for="m-keywords">Meta Keywords</label>
                    <textarea class="form-control" id="m-keywords" rows="5"></textarea>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="form-control-label" for="m-robots">Meta Robots</label>
                    <textarea class="form-control" id="m-robots" rows="5"></textarea>
                  </div>
                </div>
              </div>
               <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="form-control-label" for="schema-code">Schema Code</label>
                    <textarea class="form-control" id="schema-code" rows="5"></textarea>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="form-control-label" for="google-analytics">Google Analytics</label>
                    <textarea class="form-control" id="google-analytics" rows="5"></textarea>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <div class="form-group">
                      <label class="form-control-label">Active</label>
                      <label class="custom-toggle">
                        <input type="checkbox">
                        <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                </div>
                <div class="col-md-6">                  
                  <button type="button" class="btn btn-outline-success float-right">Save</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- Page content -->
      <div class="container-fluid mt--6">
        
        
        
        <?php //include __DIR__.'/../../../../views/includes/footer.php' ?>
      </div>
    </div>
    <!-- Argon Scripts -->
    <!-- Core -->
    <?php include __DIR__.'/../../../../views/includes/scripts.php' ?>
    <script>
      $(document).ready(function() {
  $('#summernote').summernote();
});
    </script>
  </body>
</html>