<?php include __DIR__.'/../../../../views/includes/header.php' ?>
<body>
  <?php include __DIR__.'/../../../../views/includes/sidebar.php' ?>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <?php include __DIR__.'/../../../../views/includes/topbar.php' ?>
    <!-- Header -->
    <!-- Header -->
    <div class="col-md-12">
      <div class="card-wrapper">
        <div class="card">
          <!-- Card header -->
          <div class="card-header">
            <h3 class="mb-0">Contact Details</h3>
          </div>
          <!-- Card body -->
          <div class="card-body">
            <form>
              <div class="row">
                
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="form-control-label">Email Icon</label>
                    <input class="form-control form-control-sm" type="text" placeholder="<i class='fa fa-address-book'></i>">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="form-group">
                      <label class="form-control-label">Email</label>
                      <input class="form-control form-control-sm" type="email" placeholder="info@site.com">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="form-control-label">Phone Icon</label>
                    <input class="form-control form-control-sm" type="text" placeholder="<i class='fa fa-address-book'></i>">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="form-group">
                      <label class="form-control-label">Phone</label>
                      <input class="form-control form-control-sm" type="email" placeholder="+01-4534-4534">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="form-control-label" for="disclaimer">Disclaimer</label>
                    <textarea class="form-control" id="disclaimer" rows="2"></textarea>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <div class="form-group">
                      <label class="form-control-label">Active</label>
                      <label class="custom-toggle">
                        <input type="checkbox">
                        <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                </div>
                <div class="col-md-6">                  
                  <button type="button" class="btn btn-outline-success float-right">Save</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- Page content -->
      <div class="container-fluid mt--6">
        
        
        
        <?php //include __DIR__.'/../../../../views/includes/footer.php' ?>
      </div>
    </div>
    <!-- Argon Scripts -->
    <!-- Core -->
    <?php include __DIR__.'/../../../../views/includes/scripts.php' ?>
  </body>
</html>