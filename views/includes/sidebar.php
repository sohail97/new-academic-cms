<!-- Sidenav -->
<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
  <div class="scrollbar-inner">
    <!-- Brand -->
    <div class="sidenav-header d-flex align-items-center">
      <a class="navbar-brand" href="<?php echo $base_url; ?>">
        <img src="<?php echo $base_url; ?>assets/img/brand/logo (1).png" class="navbar-brand-img" alt="...">
      </a>
      <div class="ml-auto">
        <!-- Sidenav toggler -->
        <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
          <div class="sidenav-toggler-inner">
            <i class="sidenav-toggler-line"></i>
            <i class="sidenav-toggler-line"></i>
            <i class="sidenav-toggler-line"></i>
          </div>
        </div>
      </div>
    </div>
    <div class="navbar-inner">
      <!-- Collapse -->
      <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Nav items -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo $base_url; ?>pages/widgets.html">
              <i class="ni ni-settings-gear-65 text-dark"></i>
              <span class="nav-link-text">Dashboard</span>
            </a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link active" href="#navbar-dashboards" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-dashboards">
              <i class="ni ni-shop text-primary"></i>
              <span class="nav-link-text">Dashboards</span>
            </a>
            <div class="collapse show" id="navbar-dashboards">
              <ul class="nav nav-sm flex-column">
                <li class="nav-item">
                  <a href="<?php echo $base_url; ?>pages/dashboards/dashboard.html" class="nav-link">Dashboard</a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo $base_url; ?>pages/dashboards/alternative.html" class="nav-link">Alternative</a>
                </li>
              </ul>
            </div>
          </li> -->
       
            <li class="nav-item">
            <a class="nav-link" href="#nav-menu" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="nav-menu">
              <i class="ni ni-settings-gear-65 text-dark"></i>
              <span class="nav-link-text">Navigation</span>
            </a>
            <div class="collapse" id="nav-menu">
              <ul class="nav nav-sm flex-column">
                <li class="nav-item">
                  <a class="nav-link" href="#header-dropdown" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="header-dropdown">
                    <i class="ni ni-settings-gear-65 text-dark"></i>
                    <span class="nav-link-text">Header Menu</span>
                  </a>
                  <div class="collapse" id="header-dropdown">
                    <ul class="nav nav-sm flex-column">
                      <li class="nav-item">
                        <a href="<?php echo $base_url; ?>views/admin/navigation/header/index.php" class="nav-link">Manage</a>
                      </li>
                      <li class="nav-item">
                        <a href="<?php echo $base_url; ?>views/admin/navigation/header/form.php" class="nav-link">Add New</a>
                      </li>
                      
                      <!--  <li class="nav-item">
                        <a href="#navbar-multilevel" class="nav-link" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-multilevel">Multi level</a>
                        <div class="collapse show" id="navbar-multilevel" style="">
                          <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                              <a href="#!" class="nav-link ">Third level menu</a>
                            </li>
                            <li class="nav-item">
                              <a href="#!" class="nav-link ">Just another link</a>
                            </li>
                            <li class="nav-item">
                              <a href="#!" class="nav-link ">One last link</a>
                            </li>
                          </ul>
                        </div>
                      </li> -->
                    </ul>
                  </div>
                </li>
     <li class="nav-item">
                  <a class="nav-link" href="#footer-dropdown" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="footer-dropdown">
                    <i class="ni ni-settings-gear-65 text-dark"></i>
                    <span class="nav-link-text">Footer Menu</span>
                  </a>
                  <div class="collapse" id="footer-dropdown">
                    <ul class="nav nav-sm flex-column">
                      <li class="nav-item">
                        <a href="<?php echo $base_url; ?>views/admin/navigation/footer/index.php" class="nav-link">Manage</a>
                      </li>
                      <li class="nav-item">
                        <a href="<?php echo $base_url; ?>views/admin/navigation/footer/form.php" class="nav-link">Add New</a>
                      </li>
                      
                      <!--  <li class="nav-item">
                        <a href="#navbar-multilevel" class="nav-link" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-multilevel">Multi level</a>
                        <div class="collapse show" id="navbar-multilevel" style="">
                          <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                              <a href="#!" class="nav-link ">Third level menu</a>
                            </li>
                            <li class="nav-item">
                              <a href="#!" class="nav-link ">Just another link</a>
                            </li>
                            <li class="nav-item">
                              <a href="#!" class="nav-link ">One last link</a>
                            </li>
                          </ul>
                        </div>
                      </li> -->
                    </ul>
                  </div>
                </li>
   
                
                
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#navbar-components" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-components">
              <i class="ni ni-settings-gear-65 text-dark"></i>
              <span class="nav-link-text">Pages</span>
            </a>
            <div class="collapse" id="navbar-components">
              <ul class="nav nav-sm flex-column">
                <li class="nav-item">
                  <a href="<?php echo $base_url; ?>views/admin/header/page/index.php" class="nav-link">All Pages</a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo $base_url; ?>views/admin/header/page/form.php" class="nav-link">Add New</a>
                </li>
                
                <li class="nav-item">
                  <a href="#navbar-multilevel" class="nav-link" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-multilevel">Multi level</a>
                  <div class="collapse show" id="navbar-multilevel" style="">
                    <ul class="nav nav-sm flex-column">
                      <li class="nav-item">
                        <a href="#!" class="nav-link ">Third level menu</a>
                      </li>
                      <li class="nav-item">
                        <a href="#!" class="nav-link ">Just another link</a>
                      </li>
                      <li class="nav-item">
                        <a href="#!" class="nav-link ">One last link</a>
                      </li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#navbar-forms" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-forms">
              <i class="ni ni-settings-gear-65 text-dark"></i>
              <span class="nav-link-text">Media</span>
            </a>
            <div class="collapse" id="navbar-forms">
              <ul class="nav nav-sm flex-column">
                <li class="nav-item">
                  <a href="<?php echo $base_url; ?>pages/forms/elements.html" class="nav-link">Gallery</a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo $base_url; ?>pages/forms/components.html" class="nav-link">Add Item</a>
                </li>
                
              </ul>
            </div>
          </li>
             <li class="nav-item">
            <a class="nav-link" href="#navbar-examples" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-examples">
              <i class="ni ni-settings-gear-65 text-dark"></i>
              <span class="nav-link-text">Miscellaneous</span>
            </a>
            <div class="collapse" id="navbar-examples">
              <ul class="nav nav-sm flex-column">
                <li class="nav-item">
                  <a class="nav-link" href="#logo-dropdown" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="logo-dropdown">
                    <i class="ni ni-settings-gear-65 text-dark"></i>
                    <span class="nav-link-text">Logo</span>
                  </a>
                  <div class="collapse" id="logo-dropdown">
                    <ul class="nav nav-sm flex-column">
                      <li class="nav-item">
                        <a href="<?php echo $base_url; ?>views/admin/header/logo/index.php" class="nav-link">Manage</a>
                      </li>
                      <li class="nav-item">
                        <a href="<?php echo $base_url; ?>views/admin/header/logo/form.php" class="nav-link">Add New</a>
                      </li>
                      
                      <!--  <li class="nav-item">
                        <a href="#navbar-multilevel" class="nav-link" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-multilevel">Multi level</a>
                        <div class="collapse show" id="navbar-multilevel" style="">
                          <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                              <a href="#!" class="nav-link ">Third level menu</a>
                            </li>
                            <li class="nav-item">
                              <a href="#!" class="nav-link ">Just another link</a>
                            </li>
                            <li class="nav-item">
                              <a href="#!" class="nav-link ">One last link</a>
                            </li>
                          </ul>
                        </div>
                      </li> -->
                    </ul>
                  </div>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#slider-dropdown" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="slider-dropdown">
                    <i class="ni ni-settings-gear-65 text-dark"></i>
                    <span class="nav-link-text">Slider</span>
                  </a>
                  <div class="collapse" id="slider-dropdown">
                    <ul class="nav nav-sm flex-column">
                      <li class="nav-item">
                        <a href="<?php echo $base_url; ?>pages/components/buttons.html" class="nav-link">Manage</a>
                      </li>
                      <li class="nav-item">
                        <a href="<?php echo $base_url; ?>pages/components/cards.html" class="nav-link">Add New</a>
                      </li>
                      
                      <!--  <li class="nav-item">
                        <a href="#navbar-multilevel" class="nav-link" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-multilevel">Multi level</a>
                        <div class="collapse show" id="navbar-multilevel" style="">
                          <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                              <a href="#!" class="nav-link ">Third level menu</a>
                            </li>
                            <li class="nav-item">
                              <a href="#!" class="nav-link ">Just another link</a>
                            </li>
                            <li class="nav-item">
                              <a href="#!" class="nav-link ">One last link</a>
                            </li>
                          </ul>
                        </div>
                      </li> -->
                    </ul>
                  </div>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#contact-dropdown" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="contact-dropdown">
                    <i class="ni ni-settings-gear-65 text-dark"></i>
                    <span class="nav-link-text">Contact</span>
                  </a>
                  <div class="collapse" id="contact-dropdown">
                    <ul class="nav nav-sm flex-column">
                      <li class="nav-item">
                        <a href="<?php echo $base_url; ?>views/admin/header/contact/index.php" class="nav-link">Manage</a>
                      </li>
                      <li class="nav-item">
                        <a href="<?php echo $base_url; ?>views/admin/header/contact/form.php" class="nav-link">Add New</a>
                      </li>
                      
                      <!--  <li class="nav-item">
                        <a href="#navbar-multilevel" class="nav-link" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-multilevel">Multi level</a>
                        <div class="collapse show" id="navbar-multilevel" style="">
                          <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                              <a href="#!" class="nav-link ">Third level menu</a>
                            </li>
                            <li class="nav-item">
                              <a href="#!" class="nav-link ">Just another link</a>
                            </li>
                            <li class="nav-item">
                              <a href="#!" class="nav-link ">One last link</a>
                            </li>
                          </ul>
                        </div>
                      </li> -->
                    </ul>
                  </div>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#social-dropdown" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="social-dropdown">
                    <i class="ni ni-settings-gear-65 text-dark"></i>
                    <span class="nav-link-text">Social Links</span>
                  </a>
                  <div class="collapse" id="social-dropdown">
                    <ul class="nav nav-sm flex-column">
                      <li class="nav-item">
                        <a href="<?php echo $base_url; ?>views/admin/header/social/index.php" class="nav-link">Manage</a>
                      </li>
                      <li class="nav-item">
                        <a href="<?php echo $base_url; ?>views/admin/header/social/form.php" class="nav-link">Add New</a>
                      </li>
                      
                      <!--  <li class="nav-item">
                        <a href="#navbar-multilevel" class="nav-link" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-multilevel">Multi level</a>
                        <div class="collapse show" id="navbar-multilevel" style="">
                          <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                              <a href="#!" class="nav-link ">Third level menu</a>
                            </li>
                            <li class="nav-item">
                              <a href="#!" class="nav-link ">Just another link</a>
                            </li>
                            <li class="nav-item">
                              <a href="#!" class="nav-link ">One last link</a>
                            </li>
                          </ul>
                        </div>
                      </li> -->
                    </ul>
                  </div>
                </li>
                
                
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#navbar-tables" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-tables">
              <i class="ni ni-settings-gear-65 text-dark"></i>
              <span class="nav-link-text">Uploads</span>
            </a>
            <div class="collapse" id="navbar-tables">
              <ul class="nav nav-sm flex-column">
                <li class="nav-item">
                  <a href="<?php echo $base_url; ?>pages/tables/tables.html" class="nav-link">Tables</a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo $base_url; ?>pages/tables/sortable.html" class="nav-link">Sortable</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#navbar-maps" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-maps">
              <i class="ni ni-settings-gear-65 text-dark"></i>
              <span class="nav-link-text">Users</span>
            </a>
            <div class="collapse" id="navbar-maps">
              <ul class="nav nav-sm flex-column">
                <li class="nav-item">
                  <a href="<?php echo $base_url; ?>pages/maps/google.html" class="nav-link">All Users</a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo $base_url; ?>pages/maps/vector.html" class="nav-link">Create</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo $base_url; ?>views/admin/logs/index.php">
              <i class="ni ni-settings-gear-65 text-dark"></i>
              <span class="nav-link-text">Logs</span>
            </a>
          </li>
          
        </ul>
        <!-- Divider -->
        <!-- <hr class="my-3"> -->
        <!-- Heading -->
        <!-- <h6 class="navbar-heading p-0 text-muted">Documentation</h6> -->
        <!-- Navigation -->
        <!--     <ul class="navbar-nav mb-md-3">
          <li class="nav-item">
            <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/getting-started/overview.html" target="_blank">
              <i class="ni ni-spaceship"></i>
              <span class="nav-link-text">Getting started</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/foundation/colors.html" target="_blank">
              <i class="ni ni-palette"></i>
              <span class="nav-link-text">Foundation</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/components/alerts.html" target="_blank">
              <i class="ni ni-ui-04"></i>
              <span class="nav-link-text">Components</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/plugins/charts.html" target="_blank">
              <i class="ni ni-chart-pie-35"></i>
              <span class="nav-link-text">Plugins</span>
            </a>
          </li>
        </ul> -->
      </div>
    </div>
  </div>
</nav>