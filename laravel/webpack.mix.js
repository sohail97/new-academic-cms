const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js(
    [
        "public/assets/vendor/jquery/dist/jquery.min.js",
        "public/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js",
        "public/assets/js/argon.js",
        "public/assets/js/demo.min.js",
        "public/assets/js/main.js"
    ],
    "public/js/app.js"
);

// SASS
mix.sass("resources/sass/app.scss", "public/css");
