@extends('layouts.app')

@section('content')
<div class="container-fluid mt--6">
<div class="row">
    <div class="col-xl-8">
        <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h3 class="mb-0">Brand Info</h3>
            </div>
            <!-- Card body -->
            <div class="card-body">
                <form>
                  <div class="form-group">
                    <label class="form-control-label" for="exampleFormControlInput1">Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                  </div>
                  <div class="form-group">
                    <label class="form-control-label" for="exampleFormControlTextarea1">Auth Key</label>
                    <div class="input-group input-group-merge">
                      <input class="form-control" placeholder="Password" type="password">
                      <div class="input-group-append">
                        <span class="input-group-text"><i class="fas fa-eye"></i></span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="form-control-label" for="exampleFormControlInput1">Domain ID</label>
                    <input type="number" class="form-control" id="domain_id" name="domain_id" placeholder="Domain ID">
                  </div>
                  <div class="form-group">
                    <label class="form-control-label" for="exampleFormControlInput1">Code</label>
                    <input type="text" class="form-control" id="code" name="code" placeholder="Code">
                  </div>
                  <div class="form-group">
                    <label class="form-control-label" for="exampleFormControlTextarea1">Analytics Code</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 72px;"></textarea>
                  </div>
                  <label class="form-control-label" for="exampleFormControlTextarea1">Satus</label>
                  <div class="form-group">
                    <label class="custom-toggle custom-toggle-dark">
                        <input type="checkbox" checked="">
                        <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                    </label>
                  </div>
                  <button type="button" class="btn btn-dark">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-xl-4">
        <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h3 class="mb-0">Images</h3>
            </div>
            <!-- Card body -->
            <div class="card-body">
              <!-- Single -->
              <div class="dropzone dropzone-single mb-3 dz-clickable" data-toggle="dropzone" data-dropzone-url="http://">
                <div class="dz-preview dz-preview-single"></div>
                <div class="dz-dark dz-message"><span>Brand Logo</span></div>
              </div>
                
              <!-- Multiple -->
              <div class="dropzone dropzone-multiple dz-clickable" data-toggle="dropzone" data-dropzone-multiple="" data-dropzone-url="http://">
                <ul class="dz-preview dz-preview-multiple list-group list-group-lg list-group-flush"></ul>
                <div class="dz-dark dz-message"><span>Brand Icon</span></div></div>
              </div>
        </div>
    </div>
</div>
@endsection
