<?php include 'views/includes/header.php' ?>
<body>
  <?php include 'views/includes/sidebar.php' ?>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <?php include 'views/includes/topbar.php' ?>
    <!-- Header -->
    <!-- Header -->
 
    <!-- Page content -->
    <div class="container-fluid mt--6">
     
      
      
      <?php include 'views/includes/footer.php' ?>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <?php include 'views/includes/scripts.php' ?>
</body>
</html>